# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Curate life's moments.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/vestige/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
import frontmatter
import hashlib
import os
import requests
import string
import subprocess


class Command(BaseCommand):
    help = "Import photos from Photoprism or Immich into Vestige"

    def handle(self, *args, **options):
        if settings.PHOTOPRISM_URL:
            self._importphotoprism()
        elif settings.IMMICH_URL:
            self._importimmich()
        elif settings.PHOTO_DIR:
            self._importfs()
        else:
            raise CommandError("Neither Photoprism nor Immich settings were "
                               "provided.")

        self.stdout.write(
            self.style.SUCCESS('Successful')
        )

    def _importphotoprism(self):
        # Login, obtain session ID
        resp = requests.post(settings.PHOTOPRISM_URL + 'api/v1/session',
                             headers={"Content-Type": "application/json"},
                             json={"username": settings.PHOTOPRISM_USERNAME,
                                   "password": settings.PHOTOPRISM_PASSWORD}
                             )
        sessionId = resp.headers["X-Session-Id"]
        resp = requests.get(settings.PHOTOPRISM_URL + 'api/v1/photos',
                            headers={"X-Session-ID": sessionId},
                            params={"count": "100"},
                            )
        resp_dict = resp.json()

        for photo in resp_dict:
            # The sidecar entries seem to be the thumbnails but we can work
            # back from them to the originals
            if photo["FileRoot"] != "sidecar":
                continue
            # Photoprism stores thumbs by hash and uses first three chars as
            # dirs.
            phash = photo["Hash"]
            hashpath = os.path.join(phash[0], phash[1], phash[2])

            # Chop off the thumnail's final '.jpg' to get back to originals
            # filename with [0:-4]
            self._createphotometadata(os.path.join("originals",
                                                   photo["FileName"][0:-4]),
                                      photo["OriginalName"],
                                      os.path.join("storage",
                                                   "cache",
                                                   "thumbnails",
                                                   hashpath,
                                                   phash +
                                                   "_500x500_center.jpg"))
            self.stdout.write(str(photo))

    def _importimmich(self):
        # Login, obtain access token
        resp = requests.post(settings.IMMICH_URL + 'api/auth/login',
                             headers={"Content-Type": "application/json"},
                             json={"email": settings.IMMICH_USERNAME,
                                   "password": settings.IMMICH_PASSWORD}
                             )
        resp_dict = resp.json()
        accessToken = resp_dict["accessToken"]

        # Fetch all assets
        resp = requests.get(settings.IMMICH_URL + 'api/asset',
                            headers={"Content-Type": "application/json",
                                     "Authorization": ("Bearer " +
                                                       accessToken)},
                            )
        resp_dict = resp.json()

        # Foreach photo
        for item in resp_dict:
            self._createphotometadata(item["originalPath"],
                                      item["originalFileName"],
                                      item["webpPath"])
            self.stdout.write(item["originalPath"])

    def _importfs(self):
        thumbsdir = os.path.join(settings.PHOTO_DIR, "thumbs")

        if not os.path.exists(thumbsdir):
            os.makedirs(thumbsdir, exist_ok=True)

        for root, dirs, files in os.walk(settings.PHOTO_DIR):
            if os.path.commonpath([root, thumbsdir]) == thumbsdir:
                continue

            for filename in files:
                relativedir = root.replace(settings.PHOTO_DIR, "")
                path = os.path.join(relativedir, filename)
                thumbdir = os.path.join(thumbsdir, relativedir)
                thumbfile = os.path.join(thumbdir, filename)

                if not os.path.exists(thumbdir):
                    os.makedirs(thumbdir, exist_ok=True)

                if not os.path.exists(thumbfile):
                    args = ["convert", os.path.join(root, filename),
                            "-thumbnail", "500x500", thumbfile]
                    subprocess.run(args)

                self._createphotometadata(path, filename,
                                          thumbfile.replace(settings.PHOTO_DIR,
                                                            ""))
                self.stdout.write(os.path.join(root, filename))

    def _createphotometadata(self, path, originalfilename, thumbnail):
        initial = {"roots": {}, "leaves": {}}
        post = frontmatter.Post("")
        post["tree"] = initial
        treemetadata = os.path.join(settings.STORAGE_DIR, "README.md")

        if os.path.exists(treemetadata):
            with open(treemetadata, "r") as f:
                post = frontmatter.load(f, tree=initial)

        currentnode = post["tree"]["roots"]
        pathsofar = settings.PHOTO_DIR

        for segment in path.split(os.sep):
            # Make safe version of segment. But hash should be based on full
            # path to avoid collisions.
            # https://en.wikipedia.org/wiki/URL_encoding
            allowable = string.ascii_letters + string.digits + "-_.~"
            allowablename = "".join([c for c in segment if c in allowable])

            pathsofar = os.path.join(pathsofar, segment)
            # https://stackoverflow.com/a/62699124/28170
            hash = hashlib.shake_256(pathsofar.encode()).hexdigest(5)

            if os.path.isfile(pathsofar):
                name = f"photo-{hash}-{allowablename}.md"
                self._makephotometadata(name, path, originalfilename,
                                        thumbnail)
            else:
                name = f"folder-{hash}-{allowablename}.md"
                self._makefoldermetadata(name, segment)

            currentnode[name] = dict()

            if name not in post["tree"]["leaves"]:
                post["tree"]["leaves"][name] = dict()

            currentnode = post["tree"]["leaves"][name]

        with open(treemetadata, "w") as f:
            f.write(frontmatter.dumps(post))

    def _makefoldermetadata(self, name, realname):
        foldermetadata = os.path.join(settings.STORAGE_DIR, name)
        if not os.path.exists(name):
            post = frontmatter.Post("")
            post["name"] = realname
            with open(foldermetadata, "w") as f:
                f.write(frontmatter.dumps(post))

    def _makephotometadata(self, name, path, originalfilename, thumbnail):
        photometadata = os.path.join(settings.STORAGE_DIR, name)
        post = frontmatter.Post("")

        if os.path.exists(photometadata):
            with open(photometadata, "r") as f:
                post = frontmatter.load(f)
        else:
            os.makedirs(os.path.dirname(photometadata), exist_ok=True)

        post["path"] = os.path.join(path)
        post["originalfilename"] = originalfilename
        post["dateadded"] = datetime.utcnow().isoformat()
        if "thumbnails" not in post:
            post["thumbnails"] = []

        post["thumbnails"] = list(set().union(post["thumbnails"],
                                              [thumbnail]))

        with open(photometadata, "w") as f:
            f.write(frontmatter.dumps(post))
