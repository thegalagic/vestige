# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Curate life's moments.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/vestige/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.http import url_has_allowed_host_and_scheme
from django.utils.encoding import iri_to_uri

# For Matrix image uploads
from asgiref.sync import async_to_sync, sync_to_async
import aiofiles.os
import magic
import os
from nio import AsyncClient, UploadResponse
from PIL import Image

import frontmatter
import hashlib
import time
import string


@sync_to_async
@login_required
@async_to_sync
async def view(request, path=None):
    if request.method == "GET":

        photos = []
        context = {}

        if path:
            vestigeid = path.split("/")[-1]
            p = getthing(vestigeid)
            context["form"] = {'description': p['description']}

            if vestigeid.startswith("photo"):
                context["feature"] = p
            else:
                photos = listthing(vestigeid)
        elif "type" in request.GET:
            photos = getrootthings(request.GET["type"])
        else:
            photos = getanythings("photo")

        context["photos"] = photos
        context["albums"] = getanythings("album")
        context["people"] = getanythings("people")

        return render(request, 'vestige/index.html', context)
    elif request.method == "POST":
        if "action" in request.POST:
            if request.POST["action"] == "remove":
                ids = getphotoids(request)
                if path:
                    vestigeid = path.split("/")[-1]
                    removethings(ids, vestigeid)
                else:
                    removethings(ids)
            if request.POST["action"] == "update":
                path = path.split("/")[-1]
                updatedescription(path, request.POST["photodesc"])
            if request.POST["action"] == "add":
                ids = getphotoids(request)

                if "new" in request.POST and request.POST["new"] != "":
                    # Convert new to a safe segment name
                    # https://en.wikipedia.org/wiki/URL_encoding
                    allowable = string.ascii_letters + string.digits + "-_.~"
                    allowablename = "".join([c for c in request.POST["new"] if
                                             c in allowable])
                    addnewthingtotree(allowablename, request.POST["new"],
                                      request.POST["radioType"].lower(), ids)

                elif "existing" in request.POST:
                    updatethingintree(request.POST['existing'], ids)
            if request.POST["action"] == "chat":
                ids = getphotoids(request)
                for photoid in ids:
                    p = getthing(photoid)
                    await matrixsendimage(os.path.join(settings.PHOTO_DIR,
                                                       p['path']))

        return HttpResponseRedirect("#")


# ------------------------------------------------------------------------------
# Request Utilities
# ------------------------------------------------------------------------------

def getphotoids(request):
    photoids = []
    for key in request.POST:
        if key[:5] == "check":
            photoids.append(request.POST[key])
    return photoids


# ------------------------------------------------------------------------------
# Reading from storage
# ------------------------------------------------------------------------------

def listthing(vestigeid):
    metadatafiles = []

    with open(os.path.join(settings.STORAGE_DIR, "README.md"), "r") as f:
        post = frontmatter.load(f)

    if vestigeid in post["tree"]["leaves"]:
        leaf = post["tree"]["leaves"][vestigeid]
        for entry in leaf:
            metadatafiles.append(os.path.join(settings.STORAGE_DIR, entry))

    return readmetadatafiles(metadatafiles)


def getrootthings(type):
    metadatafiles = []
    with open(os.path.join(settings.STORAGE_DIR, "README.md"), "r") as f:
        post = frontmatter.load(f)
    for key in post["tree"]["roots"]:
        if key.startswith(type):
            metadatafiles.append(os.path.join(settings.STORAGE_DIR, key))
    return readmetadatafiles(metadatafiles)


def getanythings(type):
    metadatafiles = []
    with open(os.path.join(settings.STORAGE_DIR, "README.md"), "r") as f:
        post = frontmatter.load(f)
    for key in post["tree"]["leaves"]:
        if key.startswith(type):
            metadatafiles.append(os.path.join(settings.STORAGE_DIR, key))
    return readmetadatafiles(metadatafiles)


def getthing(id):
    metadatafiles = os.path.join(settings.STORAGE_DIR, id)
    return readmetadatafiles([metadatafiles])[0]


def readmetadatafiles(metadatafiles):
    results = []

    # Load the tree
    with open(os.path.join(settings.STORAGE_DIR, "README.md"), "r") as f:
        tree = frontmatter.load(f)["tree"]

    for metadata in metadatafiles:
        with open(metadata, "r") as f:
            type = os.path.split(metadata)[1].split("-")[0]
            post = frontmatter.load(f)

            resultid = os.path.split(metadata)[1]
            title = "UNSET"
            thumbnail = []
            if type == "album":
                title = post["name"]
                # TODO: Their thumbnail is the first photo underneath them
            elif type == "photo":
                title = post["originalfilename"]
                thumbnail = (f"{settings.STATICFILES_DIRS[0][0]}/" +
                             f"{post['thumbnails'][0]}")
            elif type == "people":
                title = post["name"]
            elif type == "folder":
                title = post["name"]

            result = {"id": resultid,
                      "title": title,
                      "description": post.content,
                      "thumbnail": thumbnail}

            if type == "photo":
                result["path"] = post['path']

            result["backlinks"] = getbacklinks(tree,
                                               os.path.split(metadata)[1])

            results.append(result)
    return results


def getbacklinks(tree, vestigeid):
    backlinks = []
    for leaf in tree["leaves"]:
        if vestigeid in tree["leaves"][leaf]:
            with open(os.path.join(settings.STORAGE_DIR, leaf), "r") as f:
                name = frontmatter.load(f)["name"]

            backlink = {"id": leaf,
                        "type": leaf.split("-")[0],
                        "name": name}
            backlinks.append(backlink)
    return backlinks


# ------------------------------------------------------------------------------
# Writing to storage
# ------------------------------------------------------------------------------

def updatedescription(id, description):
    metadata = os.path.join(settings.STORAGE_DIR, id)
    post = frontmatter.Post("")

    if os.path.exists(metadata):
        with open(metadata, "r") as f:
            post = frontmatter.load(f)
    else:
        os.makedirs(os.path.dirname(metadata), exist_ok=True)

    post.content = description

    with open(metadata, "w") as f:
        f.write(frontmatter.dumps(post))


def addnewthingtotree(new, name, type, ids):
    # TODO: we must check for collisions also tho
    # TODO: don't just use albumnew on the fs, we should stdz name

    # TODO: Check input
    if type == "person":
        type = "people"

    newstamped = f"{new}{time.time()}"
    # https://stackoverflow.com/a/62699124/28170
    hash = hashlib.shake_256(newstamped.encode()).hexdigest(5)
    path = f"{type}-{hash}-{new}.md"

    # Create the metadata
    metadata = os.path.join(settings.STORAGE_DIR, path)
    if not os.path.exists(metadata):
        post = frontmatter.Post("")
        # TODO: Here we need to fill in the right metadata
        # depending on what we've just created
        post["name"] = name
        with open(metadata, "w") as f:
            f.write(frontmatter.dumps(post))

    # Update tree
    metadata = os.path.join(settings.STORAGE_DIR, "README.md")
    with open(metadata, "r") as f:
        post = frontmatter.load(f)
    # Put in the tree at root
    post["tree"]["roots"][path] = dict()
    # Put all the ids underneath in leaves
    post["tree"]["leaves"][path] = {x: {} for x in ids}

    with open(metadata, "w") as f:
        f.write(frontmatter.dumps(post))


def updatethingintree(name, ids):
    # Update tree
    metadata = os.path.join(settings.STORAGE_DIR, "README.md")
    with open(metadata, "r") as f:
        post = frontmatter.load(f)
    # Get current children
    current = post["tree"]["leaves"][name]
    # Add all the ids
    post["tree"]["leaves"][name] = current | {x: {} for x in ids}

    with open(metadata, "w") as f:
        f.write(frontmatter.dumps(post))


def removethings(ids, name=None):
    metadata = os.path.join(settings.STORAGE_DIR, "README.md")
    with open(metadata, "r") as f:
        post = frontmatter.load(f)

    if name:
        current = post["tree"]["leaves"][name]
    else:
        current = post["tree"]["roots"]
    for id in ids:
        if id in current:
            del current[id]

    safelist = set()
    for leaf in post["tree"]["leaves"]:
        safelist = safelist | set(post["tree"]["leaves"][leaf].keys())
    for root in post["tree"]["roots"]:
        safelist = safelist | {root}

    kills = set(post["tree"]["leaves"].keys()) - safelist
    for kill in kills:
        del post["tree"]["leaves"][kill]

    with open(metadata, "w") as f:
        f.write(frontmatter.dumps(post))


# ------------------------------------------------------------------------------
# Matrix Send Image
# ------------------------------------------------------------------------------

async def matrixsendimage(path):
    homeserver = settings.MATRIX_HOMESERVER
    matrixid = settings.MATRIX_ID
    password = settings.MATRIX_PASSWORD
    client = await matrixlogin(homeserver, matrixid, password)
    await matrixuploadimage(client, path)
    await client.close()


async def matrixlogin(homeserver, matrixid, password) -> None:
    client = AsyncClient(homeserver, matrixid)
    await client.login(password)
    return client


async def matrixuploadimage(client, image):
    mime_type = magic.from_file(image, mime=True)
    if not mime_type.startswith("image/"):
        raise Exception(("Drop message because file does not "
                         "have an image mime type."))

    im = Image.open(image)
    (width, height) = im.size

    file_stat = await aiofiles.os.stat(image)
    async with aiofiles.open(image, "r+b") as f:
        resp, maybe_keys = await client.upload(
            f,
            content_type=mime_type,
            filename=os.path.basename(image),
            filesize=file_stat.st_size,
        )
    if not isinstance(resp, UploadResponse):
        raise Exception(f"Failed to upload image. Failure response: {resp}")

    content = {
        "body": os.path.basename(image),  # descriptive title
        "info": {
            "size": file_stat.st_size,
            "mimetype": mime_type,
            "thumbnail_info": None,
            "w": width,
            "h": height,
            "thumbnail_url": None,
        },
        "msgtype": "m.image",
        "url": resp.content_uri,
    }

    await client.room_send(settings.MATRIX_ROOM_ID,
                           message_type="m.room.message",
                           content=content)


# ------------------------------------------------------------------------------
# Login
# ------------------------------------------------------------------------------

def check_demo_user(username):
    if settings.DEMO_USER and username == 'demo':
        if not User.objects.filter(username="demo").exists():
            User.objects.create_user("demo", 'lennon@thebeatles.com', "demo")


def login_user(request):
    # Next parameter need careful processing
    # https://stackoverflow.com/a/64154187/28170
    # https://stackoverflow.com/a/60372947/28170
    paramdict = {"registration": settings.REGISTRATION}

    if settings.DEMO_USER:
        paramdict['demo'] = "True"

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        check_demo_user(username)

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            next = request.POST['next']
            if next is None:
                return HttpResponseRedirect(reverse('vestige:view'))
            elif url_has_allowed_host_and_scheme(
                    url=next,
                    allowed_hosts={request.get_host()},
                    require_https=request.is_secure()):
                return HttpResponseRedirect(iri_to_uri(next))
            else:
                return HttpResponseRedirect(reverse('vestige:view'))
        else:
            paramdict['error'] = "Login failed"
            return render(request, 'vestige/login.html', paramdict)
    else:
        if 'next' in request.GET:
            paramdict['next'] = request.GET['next']
        return render(request, 'vestige/login.html', paramdict)


def register_user(request):
    if settings.REGISTRATION:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']

            if User.objects.filter(username=username).exists():
                return render(request, 'vestige/register.html',
                              {'error': "Username already taken"})

            user = User.objects.create_user(username, 'lennon@thebeatles.com',
                                            password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('vestige:view'))
            else:
                return HttpResponseRedirect('.')
        else:
            return render(request, 'vestige/register.html')
    else:
        return HttpResponseRedirect('.')


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('vestige:login_user'))
