<!--
SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

Curate life's moments.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/vestige/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# vestige

A self-hosted life memories manager - curate life's moments.

Vestige works with your existing photo gallery to let you:

* Add personal details like birth dates, how you met, likes etc
* Add relationships between people: parents, spouses, children, pets etc
* Share photos to your favourite messaging groups; record snippets of
  the discussion to capture memories.

All data is saved in open, textual formats to be kept forever.

Vestige is open source forever under the AGPL license. Built with Python, Django
and Bootstrap.

**Note**: this project is currently a prototype and should be used just for testing.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
  * [Configuration](#configuration)
* [Usage](#usage)
* [Current Limitations](#current-limitations)
* [Future](#future)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

I've been looking for a way to preserve my family's memories and history.
Photo Manager apps are great associating small bits of information with media
like tags, people and locations but I couldn't find anything that would let me
document the history, memories and relationships as well. I also needed to do
this over chat where I can reach more family members and preserve their memories
first-hand.

The genealogy software I looked didn't integrate well with my existing photo
gallery.

## Install

Requirements:

* If not using the container than ImageMagick is required to create thumbnails
  when importing existing photos from a plain filesystem.

### Configuration

There are a few settings you can control via environment variables:

* `STORAGE_DIR` is where vestige will keep all the metadata about your photos in
  markdown format.
* If importing from existing files:
  * `PHOTO_DIR` should point to where your photos are kept. Vestige will create
    a `thumbs` dir here but this is the only instance in which it writes to
    `PHOTO_DIR`.
* If importing from Immich:
  * `IMMICH_URL` - the base URL of your Immich instance with trailing slash,
    e.g. `http://localhost:2342/`
  * `IMMICH_USERNAME` - Immich username e.g. `admin@example.com`
  * `IMMICH_PASSWORD` - Immich password
  * `PHOTO_DIR` should be the parent dir of Immich's `UPLOAD_LOCATION`, e.g.
    `/opt/immich` if your Immich upload location is `/opt/immich/upload`.
* If importing from Photoprism:
  * `PHOTOPRISM_URL` - the base URL of your photoprism instance with trailing slash,
    e.g. `http://localhost:2342/`
  * `PHOTOPRISM_USERNAME` - photoprism username e.g. `admin`
  * `PHOTOPRISM_PASSWORD` - photoprism password
  * `PHOTO_DIR` should be the parent dir of photoprism's `originals` and
    `storage`, e.g.
    `/opt/photoprism` if your photoprism is using `/opt/photoprism/originals`
    and `/opt/photoprism/storage`. We don't yet support if there are different
    parents to these locations.
* For Matrix integration supply details of the account and room to use:
  * `MATRIX_HOMESERVER` - the account's homeserver e.g. `https://matrix-client.matrix.org`
  * `MATRIX_ID` - the account's id e.g. @example:matrix.org
  * `MATRIX_PASSWORD` - the account's password
  * `MATRIX_ROOM_ID` - the room's ID e.g. `!NNNNNNNNN:matrix.org`

## Usage

Detail how to import from file system, Immich and Photoprism.

## Current Limitations

## Future

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue or reaching out on Mastodon: @fosstian@fosstodon.org. PRs will
be considered. For full details see [CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* All source code is licensed under AGPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

Vestige is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
